<?php

namespace Serenata\PrettyPrinting;

/**
 * Pretty prints default values for parameters.
 */
class ParameterDefaultValuePrettyPrinter
{
    /**
     * @param string $value
     *
     * @return string
     */
    public function print(string $value): string
    {
        return $value;
    }
}
