<?php

namespace Serenata\Workspace\Configuration\Parsing;

/**
 * Indicates the workspace configuration has an invalid format (i.e. it isn't JSON).
 */
class WorkspaceConfigurationInvalidFormatException extends WorkspaceConfigurationParsingException
{
}
